import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import 'react-datepicker/dist/react-datepicker.css';
import * as LoginAction from '../actions/LoginAction';

class Register extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: {
        email: "",
        password: "",
        dob: moment()
      },
      people: []
    }
  }

  componentDidMount() {
    this.props.LoginAction.addPerson();
  }

  handleInputField = (event) => {
    const { user } = this.state;
    user[event.target.name] = event.target.value
    this.setState({ user })
  }

  handleDOBChange(date) {
    const { user } = this.state;
    user.dob = date
    this.setState({ user });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    console.log(" - - > > > > > > >  > ", nextProps);
    this.setState({
      people: nextProps.people
    })
  }

  render() {
    const { user } = this.state;
    return (
      <div className="App">
        Email:  <input type="email" name="email" value={user.email} onChange={(e) => this.handleInputField(e)} /> <br />
        Password:  <input type="password" name="password" value={user.password} onChange={(e) => this.handleInputField(e)} /> <br />
        <DatePicker
          selected={user.dob}
          onChange={(date) => this.handleDOBChange(date)}
        />
        {/* DOB:  <input type="date" name="dob" value={user.dob} onChange={(e)=> this.handleInputField(e)} /> */}
        {/* <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p> */}
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  console.log(" --  >  ", state)
  return {
    people: state.loginreducer[0]
  };
}

function mapDispatchToProps(dispatch) {
  return {
    LoginAction: bindActionCreators(LoginAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);
