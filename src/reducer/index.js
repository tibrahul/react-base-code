import { combineReducers } from 'redux';
import loginreducer from './LoginReducer'

const rootReducer = combineReducers({
    loginreducer
});

export default rootReducer;