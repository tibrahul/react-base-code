import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Register from './components/Register';
import './App.css';

class App extends Component {

  render() {
    return (
      <Switch>
         <Route exact path="/register" component={Register} />
      </Switch>
    );
  }
}

export default App;
